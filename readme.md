# Web Scraper

Assignment 2/3 for Experis Academy's JavaScript course. A web scraper that saves Reddit-post titles and corresponding usernames in a JSON-file.

## Getting Started
### Prerequesites

* Node.js 
* Cheerio
* Puppeteer

### Run the scraper
Open the computer console/terminal.
This command will install all needed package modules: 
> npm install 

This command will run the scraper:
> node scraper.js

## Built With

* Visual Studio Code
* Node.js
* Cheerio
* Puppeteer

## Resources

I read these tutorials:

* https://www.freecodecamp.org/news/the-ultimate-guide-to-web-scraping-with-node-js-daa2027dcd3/
* https://www.scrapingbee.com/blog/web-scraping-javascript/

## Author

* Charlotte Ødegården

## License

Still don't know enough about licenses to firmly state anything about this. 