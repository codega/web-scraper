const $ = require('cheerio');
const puppeteer = require('puppeteer');
const fs = require('fs');

const url = 'https://www.reddit.com/r/popheads/';
const usernameClass = '._2mHuuvyV9doV3zwbZPtIPG';

const jsonObject = {
    titleTable: []
};

puppeteer.launch().then(function(browser) {
    return browser.newPage();
  }).then(function(page) {
      return page.goto(url).then(function() {
          return page.content();
        });
    }).then(function(html) {
        //Finds and saves post titles
        $('h3', html).each(function() {
            const title = $(this).text();
            const titleObject = {
                "title": title
            }
            jsonObject.titleTable.push(titleObject);
          });
          //Finds and saves corresponding usernames
          let i = 0;
          $(usernameClass, html).each(function() {
              const username = $(this).text();
              const titleObject = jsonObject.titleTable[i];
              titleObject["username"] = username;
              i++;
          });
        }).then(function(){
            //Changes object into a JSON-file 
            const jsonFile = JSON.stringify(jsonObject);
            fs.writeFile("output.json", jsonFile, 'utf8', function (error) {
                if (error) {
                    return console.log(error.message);
                }
                return console.log("File has been saved.");
            });
         });

         